"""Genetic algorithm following a pycma-like interface."""
# Native imports
import time
import random
from math import ceil
from typing import Tuple
from typing import Callable
from typing import List
from statistics import mean
from statistics import stdev
from collections import defaultdict


# GA type definitions
Individual = Tuple[float]
Init = Callable[[], Individual]
Population = List[Individual]
CrossOver = Callable[[Population], Population]
Mutated = Callable[[Individual], Individual]


def identity(x): return x


def get_pareto_front_idxs(
        population: Tuple[Individual],
        values: Tuple[Tuple[float]],
        ) -> Tuple[int]:
    def dominates(i: int, j: int) -> bool:
        # i is not worse than j in any objective
        for vali, valj in zip(values[i], values[j]):
            if vali < valj:
                return False
        # i is better that j in at least one objective
        for vali, valj in zip(values[i], values[j]):
            if vali > valj:
                return True
        return False

    def points_that_dominate(i: int) -> Tuple[int]:
        """Returns the points that dominate point i."""
        return tuple((
                j for j in range(len(population))
                if dominates(j, i)
            ))

    return tuple((
            i for i in range(len(population))
            if len(points_that_dominate(i)) == 0
        ))


class GeneticAlgorithm:
    def __init__(
            self,
            population_size: int,
            elite_n: int,
            init: Init,
            mutated: Mutated = identity,
            crossover: CrossOver = identity,
            max_generations: int = None,
            max_function_evaluations: int = None,
            fitness_termination: float = None,
            ):
        self.init = init
        self.population_size = population_size
        self.max_generations = max_generations
        self.fitness_termination = fitness_termination
        self.max_function_evaluations = max_function_evaluations
        self.population = [init() for _ in range(population_size)]
        self.elite_n = elite_n
        self.log: dict[str, list] = defaultdict(list)
        self.mutated = mutated
        self.start_time = time.time()
        self.latest_elites = list()
        self.latest_elites_values = list()
        self.elite = init()
        self.pareto_front = list()
        self.elite_fitness = float("-inf")
        self.crossover = crossover

    @property
    def function_evaluations(self):
        return sum(
                [0] + [len(gen_values) for gen_values in self.log["values"]],
            )

    @property
    def generation_n(self):
        return len(self.log["values"])

    @property
    def elapsed_time(self):
        return time.time() - self.start_time

    @property
    def finished(self):
        if self.max_generations is not None\
                and self.generation_n >= self.max_generations:
            return True
        if self.fitness_termination is not None\
                and self.elite_fitness >= self.fitness_termination:
            return True
        if self.function_evaluations >= self.max_function_evaluations:
            return True
        return False

    def ask(self):
        return self.population

    def tell(self, solutions, values):
        """If values are tuples, the Pareto front is selected for mutation."""
        if isinstance(values[0], float):
            # Single objective optimization
            idx = sorted(
                    range(len(values)),
                    key=lambda i: values[i], reverse=True
                )
            solutions = [solutions[i] for i in idx]
            values = [values[i] for i in idx]
            self.latest_elites = solutions[:self.elite_n]
            if values[0] > self.elite_fitness:
                self.elite = solutions[0]
                self.elite_fitness = values[0]
        else:
            # Multi-objective optimization
            solutions = self.latest_elites + solutions
            values = self.latest_elites_values + values
            idxs = get_pareto_front_idxs(
                    solutions,
                    values,
                )
            # Limit the size of the Pareto front
            idxs = random.choices(idxs, k=self.population_size)
            self.pareto_front = [
                    solutions[i] for i in idxs
                ]
            values = [
                    values[i] for i in idxs
                ]
            self.latest_elites = self.pareto_front
            self.latest_elites_values = values
            self.elite = random.choice(self.pareto_front)

        # Mutate best solutions to form new population
        self.population = [self.elite] + list(map(
                self.mutated,
                random.choices(self.crossover(self.latest_elites), k=self.population_size-1)
            ))

        # Log statistics
        self.log["values"].append(values)

    def disp(self):
        time_seconds = self.elapsed_time
        time_minutes = int(time_seconds/60)
        time_seconds_offset = round(time_seconds - time_minutes*60, 2)
        function_evals = self.function_evaluations
        generation = self.generation_n
        values = self.log["values"][-1]
        message_dict = {
                "Generation": generation,
                "#Fevals": function_evals,
                "t[m:s]": f"{time_minutes}:{time_seconds_offset}",
            }
        if not isinstance(values[0], float):
            # Assume multiobjective
            for f_i in range(len(values[0])):
                f_i_vals = [val[f_i] for val in values]
                f_i_best = round(max(f_i_vals), 4)
                message_dict.update({
                        f"F_{f_i}_best": f_i_best,
                    })
        else:
            generation_best = round(max(values), 8)
            generation_mean = round(mean(values), 8)
            generation_std = round(stdev(values), 8)
            message_dict.update({
                    "val_best": generation_best,
                    "val_mean": generation_mean,
                    "val_std": generation_std,
                })
        header = " ".join(map(str, message_dict.keys()))
        row = "   ".join(map(str, message_dict.values()))
        if generation % 10 == 1:
            print(header)
        print(row)

    def plot_fig(self):
        """Returns a matplotlib fig object."""
        # Determine if we performed a single- or multi-
        # objective optimization.
        multiobjective = False
        if not isinstance(self.log["values"][-1][0], float):
            multiobjective = True

        # Build the metrics we will plot
        if multiobjective:
            log = dict()
            for f_i in range(len(self.log["values"][-1][0])):
                f_i_vals = [
                        sorted([val[f_i] for val in values])
                        for values in self.log["values"]
                    ]
                log.update({
                        f"F_{f_i}": f_i_vals,
                    })
        else:
            log = self.log

        import matplotlib.pyplot as plt
        default_x_inches = 6.4
        default_y_inches = 4.8
        total_plots_x = 2
        total_plots = len(log.keys())
        total_plots_y = ceil(total_plots/total_plots_x)
        figsize = (
                default_x_inches*total_plots_x,
                default_y_inches*total_plots_y,
            )
        fig, axs = plt.subplots(
                nrows=total_plots_x,
                ncols=total_plots_y,
                figsize=figsize,
                squeeze=False,
            )
        axs = [ax for axsi in axs for ax in axsi]


        # Plot the metrics
        for key, ax in zip(log.keys(), axs):
            ax.set_title(key)
            x = range(len(log[key]))
            y = log[key]
            ax.plot(x, y)
        return fig
